const { Router } = require('express');
const { productoGet, productoPut, productoPost, productoDelete} = require ('../controllers/ProductoControllers');

const router = Router();

router.get('/', productoGet);

router.put('/:id', productoPut);

router.post('/', productoPost);

router.delete('/:id', productoDelete);


module.exports = router;