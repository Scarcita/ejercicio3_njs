const { v4: uuidv4 } = require('uuid');

class Producto {
  
  id = '';
  nombres = '';
  categoria = '';
  numero_serie = '';
  fecha_caducidad = '';
  id_persona = '';

  constructor(nombres, categoria, numero_serie, fecha_caducidad, id_persona ) {
    this.id = uuidv4()
    this.nombres = nombres
    this.categoria = categoria;
    this.numero_serie = numero_serie;
    this.fecha_caducidad = fecha_caducidad;
    this.id_persona = id_persona;
  }
}

module.exports = Producto;