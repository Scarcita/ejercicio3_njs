const { guardarInfo1 } = require('../models/guardarInfo');
const Producto = require('./producto')

class Productos {

  constructor() {
    this._listado1 = [];
  }

  crearProducto(producto = {}) {
    // console.log(persona);

    this._listado1.push(producto)
    // console.log(this._listado, '9999');SI
    // this._listado[person.id] = person
    // console.log('persona', person, 'persona'); //not defined
  }

  get productosArr() {
    const listado1 = [];

    Object.keys(this._listado1).forEach(key => {
    //  console.log('llave', key, 'llave');SI
      const person = this._listado1[key];
      listado1.push(person);
    });
    // console.log('alba');
    // console.log('array', listado, 'array');SI
    return listado1;
}

cargarProductoFormArray(productos = []){

  productos.forEach(producto => {
    this._listado1[producto.id] = producto;
  }) 
} 

editarProducto(producto, index) {
  this._listado1[index].nombres = producto.nombres,
  this._listado1[index].categoria = producto.categoria,
  this._listado1[index].numero_serie = producto.numero_serie,
  this._listado1[index].fecha_caducidad = producto.fecha_caducidad,
  this._listado1[index].id_persona = producto.id_persona
  guardarInfo1(this._listado1)
}

eliminarProducto(id){
  
  this._listado1 = this._listado1.filter(data =>  {
    return data.id!== id
  })


}

}

module.exports = Productos;